Eu me anestesio
Eu me dopo
Eu nao quero mais sentir
Eu so quero sentir o que nao estou sentindo
Que sentido?
Um rumo?
Nem sei, nem sinto, so vivo

Tudo que eh muito intenso, arranca a pele, deixa a marca bem la no fundo e mesmo vc querendo esquecer, toda vez q ve ou sente no toque, a memoria daquela marca volta. O louco eh o cerebro perceber que o contexto mudou e logo deveria mudar a intepretacao

ir e vir
o caminjho que vem e vai
ir e vir
deixar e levar
ir e vir
que caminhos virao?
aonde ir
sei o que busco?
eu nunca saberei onde quero chegar
so sei de uns lugares que desejo passar
porque o destino final nao tenho como saber
por aqui caminho
por ali corro
aqui dou uma parada
e por logo ali, passo daqui a pouco

Eu tomo caminhos com colher

tell a story
this meeting keeps drining my sou. People here keep talking and talking but I cant hear. looks like my ears decidedto stop to work, they just gaved up this moment. I am affraid of not being able to interact with them anymore. What is happening with me? Am I just tired? Am I suffering from something that I don't know? What I am affraid of? Maybe one day I will know but for now, what are they takling about?


Reencontro do perdido
Reencontro o fugido
Reencontro o que nunca tive
Reencontro o que nunca foi
Encontro o que nao procuro
Encontro o que nao foi visto
Reencontro o conto
Encanto o ponto
Desencanto

Eu tive um sonho e vc tava la. Nao sei dizer ao certo como tu havia chego ou o que fazia, mas vc estava la. Somente o fato de vc estar la ja me dava tanta confianca, me reconfortava e me acalmava. Como posso colocar vc numa pilula? Como posso tomar uma dose dessa toda vez que eu estiver em panico? Como voce cultivou essa capacidade de me acalmar somente com a sua presenca? Como vc foi aparecer na minha vida assim? Nem te conhecia e parece que vc sempre esteve aqui. Agora o que me resta eh somente duvida. Vc veio, me encantou e sei tampouco sobre vc que nao me impedende te admirar mas que me causa o temor de descobrir que essa vida ja esta ocupada e que o banco, o assento nao esta livre. Posso esperar a proxima parada? Sera que da pra sentar ai?

minha caneta me picou ontem
ela nao queria parar de escrever
fui tampar e ela me mordeu
a marca ta aqui ate agora, quer ver?
ficou um azul marcado e olha que lavei a mao
caneta descontrolada, essa eh nova pra mim

quando acalmamos o ruido da mente, pode-se escutar o coracao
Descobrimos uma voz muito doce e apacible, uma chamada nao com palavras mas sim com sentimento

se nao escuto o que sinto, nao escuto meu coracao

se me deixo anestesiar com o mundo, essa anestesia nao me permitira sentir a mim mesma

porque to indo se nao quero ir?
porque nao pedi pra ficar?
porque nao me pediu pra ficar?
porque ta acontecendo do jeito que ta?
o que preciso aprender?
quando vou te ver?
quando vou me ver?
quem ou o que eu sou?
tenho que ser algo?

depende onde vai
depende onde fica
depende se fica
depende se vai
nao depende
nem pende
so pentea
ou
serpentea

meu amor por voce nao cabe dentro do peito
tive que colocar um pouco dele nesse pedaco de papel
nao sei se voce sera capaz de recebe-lo nao estando aqui e nao sendo capaz de ler essas linhas
mas eh preciso saber ler para sentir que eh amado?
alem do mais, quais as outras formas e formas onde posso depositar esse amor?
depositar? ate parede que eh tipo poupanca, onde vc deposita um pouquinho pra ficar rendendo mensalmente...
meu amor por vc nao precisa de taxa celic pra aumentar
isso acontece cada vez que olho pra vc
que te escuto, que sei como foi o seu dia e suas dificuldades
ele aumenta cada vez mais q voce me aceita, que eu te aceito
e nos vamos nos aceitando...
sera q esse amor aceita debito ou credito?
acho que aqui somente do vivo
somente escuta se for efetivo
eu queria ta ai... eu queria que vc estivesse aqui
eu te quero e me quero, nos quero juntos nessa querencia que chamam de vida, vem logo vai... voy pra ai, mas por favor, vem aqui


minhas maos deslizam
por um caminho incerto
elas nao seguem, param
anciosas e receiosas
com o que irao percorrer
nao sabem transpassar
essa barreira criada
por nao sei quem
sou eu ou se eh vc
por que isso nao eh falado?
o que tem que ser dito?
quem tem que dizer?
porque as mao nao conversaom?
e sao sempre os pes quem falam?
se tocam, se acariciam, se abrem
abrem caminhos que quero percorrer
mas onde nao posso tatear
por que fico cega por vc?
