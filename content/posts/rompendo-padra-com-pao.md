+++
title = "Rompendo padrão com pão"
date = "1906-06-06"
+++
Nos últimos tempos algo que ando exercitando é udas as coisas que faço. Sei lá,parece que a vida vai bem mas tem algo que me toco que me ripiou. Primeiro aceitação de não ter um emprego registrado, diferente do que foi toda minha trajetória trabalhistica até agora. Também tem o lance da alimentação, sempre gostei de satisfazer todos os desejos e vontades, ainda faço isso mas tá em outro lugar. Sempre tomei balde de café. Era minha água, quando não era ele, era cerveja. As vezes suco mas água quase nunca. Hoje sai de casa sem comer e sem levar algo comigo, fiquei com um destino na cabeça "chego lá e como". Comer como se o lugar tava fechado? Quantas vezes na vida isso já aconteceu antes? Ficar com algo fixo na cabeça,fechar a visão somente para esse destino e esquecer a possibilidade de olhar em volta e me surpreender com outra decisão?

Bem, depois de bater no lugar fechado, fui para a rua "caçar", que por sinal é uma das caças mais simples, não tem que esgueirar, espreitar e esperar. Saiu na rua, o que mais tem é opção de comida, só que nisso de comer, comer o que? Nesse processo de mudança, gostaria de variar mais, não me restringir ao mesmo, sair do "café-com-leite", mas a fome bagunça com a cabeça. Pulei o primeiro lugar que vi porque não me senti a vontade lá, muito "chique", do ladin um pé de chinelo que nem eu, gostei. Entro e bato direto oolhar na vitrine dos salgados, tô reduzinfo as farinhas e leite pra variar um cadin mas me enamoro pelo salgadinho, pergunto e o recheio é de catupiry, gostei.

É pão de batata, tenho uma paixão antiga por ele, alias, pensando aqui faz muito, mas muito empo que não como um. Pra acompanhar, vou de café. Mesmo querendo mudar hábitos, fui muito no mmodo automático. Comi. Bebi. Deu até formigamento na língua, dá pra sentir a acidez do café descendo e seu calorzin me aquecendo. Olho em volta e me deparo com as outras opções, quem diria! Aqui tem tapioca com preço justo, tem suco de todo tipo e não são abusivos. Nesse processo de café da manhã, fico buscando em quantas outras coisas eu acabo escolhendo assim, na "fome" e no "já" sem acalmar, observar e perceber que eu não tenho somente uma boa opção.

Todas as opções sçao boas, mas tem algo que quando garro numa somente baseada no impulso ou argumentação mental, parece que não é a dita melhor. Apesar que isso de ranquear opções já é outra brisa mental. Eu gosto da minha mente mas parece que as vezes ela mente, ou cria uma lógica que não faz muito sentido.

Enfim, comi, cuidei daquilo que prendia minha atenção e agora percebo que a atenção só tava nela porque não tinha outra coisa com que me preocupar e me distrair. O ócio é muito parceiro da vontade de comer. Parece que colocar algo para dentro pela boca, preenche esse espaço que o ócio cria. Como recriar essa relação, me alimentando de outras coisas?

Bem, essa é a minha reflexão de barriga cheia e com o privilégio de escolher o que comer. E quem não pode nem comer?

Comer quando se tem fome, dormir quando se tem sono, descansar quando o corpo pede e correr quando a energia nos leva, parecem coisas tão simples e tão básicas mas que são negadas a muita gente, por que tem que ser assim?


PS: acabei de me dar conta que somente escrevi isso porque satisfiz da forma mais direta a comida com o primeiro impulso, ou seja, o impito de escrever nasceu porque impulso. Um impulso que começou no pão com batata e acabou em tinta.
