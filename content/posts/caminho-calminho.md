+++
title = "Como saber pra onde ir?"
date = "1999-10-01"
+++

  Como saber pra onde ir quando o caminho é tão amplamente incerto e eu não consigo sentir a direção? Parte da racionalidade aponta alguns lugares e me confundo se dentro dessa racionalidade tenha alguma intuição. Como sentir? Eu observo o entorno procurando por sinais, mas eles são tão delicados que parece que meu olho procura por grandes letreiros de LED. Palavra vazia corresponde ao ponto maior de razão entre o ar, a terra, o fogo e a água.
  Caneta, siga seu caminho sem ordem, desenhe seu proposito, trace sua rota prontamente por onde precisa passar.
  Caneta, marque a sua escrita, dance pelo papel e mostre as pessoas o interior da sua tinta,essa tinta que marca,cheira e pinta o caminho.

CAMINHO
CAMINHO
CALMINHO
CAEUMIMO
CALDINHO
CAMINHO
CANTINHO
CASINHO
CADINHO
CASINHO
CALMINHO
CAMINHO
CALDINHO
CASINHO
CAMINHO
CANTINHO
CASINHO
CAMINHO
CANTINHO
CASINHO
CAMINHO
CAENINHO
